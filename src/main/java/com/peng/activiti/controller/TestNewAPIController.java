package com.peng.activiti.controller;

import com.peng.activiti.SecurityUtil;
import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.Task;
import org.activiti.api.task.model.builders.TaskPayloadBuilder;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.api.process.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api")
public class TestNewAPIController {
    @Autowired
    private ProcessRuntime processRuntime;
    @Autowired
    private TaskRuntime taskRuntime;
    @Autowired
    private SecurityUtil securityUtil;

    @GetMapping("/queryProcesses")
    @ResponseBody
    public List<String>  queryProcesses(String username){
        securityUtil.logInAs(username);
        List<String> messages = new ArrayList<>();
        Page<ProcessDefinition> processDefinitions = processRuntime.processDefinitions(Pageable.of(0,100));
        messages.add("可用的流程定义数量："+processDefinitions.getTotalItems());
        for (ProcessDefinition pd:processDefinitions.getContent()){
            messages.add("流程定义："+pd);
        }
        return messages;
    }

    @GetMapping("/startProcess")
    @ResponseBody
    public List<String> startProcess(String username,String definitionKey) {
        securityUtil.logInAs(username);
        List<String> messages = new ArrayList<>();
        ProcessInstance pi = processRuntime.start(ProcessPayloadBuilder.start().withProcessDefinitionKey(definitionKey).build());
        messages.add("创建流程实例成功");
        messages.add("流程实例ID："+pi.getId());
        return messages;
    }

    @GetMapping("/queryTasks")
    @ResponseBody
    public List<String> queryTasks(String username){
        securityUtil.logInAs(username);
        List<String> messages = new ArrayList<>();
        Page<Task> taskPage = taskRuntime.tasks(Pageable.of(0,10));
        if (taskPage.getTotalItems()>0){
            for (Task task:taskPage.getContent()){
                    messages.add("任务："+task);
            }
        }
        return messages;
    }

    @GetMapping("/completeAllTasks")
    @ResponseBody
    public void completeAllTasks(String username){
        securityUtil.logInAs(username);
        List<String> messages = new ArrayList<>();
        Page<Task> taskPage = taskRuntime.tasks(Pageable.of(0,10));
        if (taskPage.getTotalItems()>0){
            for (Task task:taskPage.getContent()){
                if (task.getAssignee()==null){
                    messages.add(username+":签收了任务："+task.getName()+",任务ID："+task.getId());
                    taskRuntime.claim(TaskPayloadBuilder.claim().withTaskId(task.getId()).build());
                }
                messages.add(username+":完成了任务："+task.getName()+",任务ID："+task.getId());
                taskRuntime.complete(TaskPayloadBuilder.complete().withTaskId(task.getId()).build());
            }
        }
    }
}
