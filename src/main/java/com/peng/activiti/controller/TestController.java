package com.peng.activiti.controller;

import com.peng.activiti.SecurityUtil;
import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.Task;
import org.activiti.api.task.model.builders.TaskPayloadBuilder;
import org.activiti.api.task.model.payloads.ReleaseTaskPayload;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * 控制器
 *  任务执行
 *      使用postman测试
 */
@Controller
@RequestMapping("")
public class TestController {
    @Autowired
    private ProcessRuntime processRuntime;
    @Autowired
    private TaskRuntime taskRuntime;
    @Autowired
    private TaskService taskService;
    @Autowired
    private SecurityUtil securityUtil;

    List<String> usernameList = Arrays.asList("user01","user02","user03","leader01","leader02","leader03");
    String process1 ="process1";
    String process2 ="process2";
    /**
     * 查看流程定义 查看某人的流程可用数量
     * http://localhost:8080/process
     */
    @GetMapping("/process")
    @ResponseBody
    public String queryProcess(String username) {
        securityUtil.logInAs(username);
        Page<ProcessDefinition> processDefinitionPage = processRuntime.processDefinitions(Pageable.of(0, 10));
        System.out.println("可用的流程定义数量：" + processDefinitionPage.getTotalItems());
        for (ProcessDefinition pd : processDefinitionPage.getContent()) {
            System.out.println("流程定义：" + pd);
        }
        Map map = new HashMap();
        map.put("Page",processDefinitionPage);
        return map.toString();
    }

    @GetMapping("/startProcess")
    @ResponseBody
    public void startProcess(String username,String processName) {
        securityUtil.logInAs(username);
        ProcessInstance p = processRuntime.start(ProcessPayloadBuilder.start().withProcessDefinitionKey(processName)
                .build());
        System.out.println("流程实例ID："+p.getId());
        System.out.println("流程实例名字："+p.getName());
        System.out.println("流程实例状态："+p.getStatus());
    }

    @GetMapping("/queryTask")
    @ResponseBody
    public void queryTask(String username) {
        System.out.println(username);
        this.queryUser2(username);
    }
    @GetMapping("/queryGroupTask")
    @ResponseBody
    public void group(String name) {
        securityUtil.logInAs(name);
        System.out.println("组的任务数量："+taskService.createTaskQuery().taskCandidateGroup(name).count());
    }

    @GetMapping("/queryAllTask")
    @ResponseBody
    public void queryTaskAll() {
        for (String username:usernameList){
            this.queryUser1(username);
        }
    }

    @GetMapping("/claim")
    @ResponseBody
    public void FinishedTask(String username,String taskId){
        securityUtil.logInAs(username);
        Task task = taskRuntime.task(taskId);
        if (task.getAssignee() == null) {
            taskRuntime.claim(TaskPayloadBuilder.claim().withTaskId(task.getId()).build());
        }
    }

    @GetMapping("/release")
    @ResponseBody
    public void setAssigneeToGroupTask(String username,String taskId){
        securityUtil.logInAs(username);
        taskRuntime.release(TaskPayloadBuilder.release().withTaskId(taskId).build());
    }

    @GetMapping("/complete")
    @ResponseBody
    public void complete(String username,String taskId){
        securityUtil.logInAs(username);
        Task task = taskRuntime.task(taskId);
        //候选人为Null 需要在前端先拾取任务
        if (task.getAssignee() == null) {
            taskRuntime.claim(TaskPayloadBuilder.claim().withTaskId(task.getId()).build());
        }
        taskRuntime.complete(TaskPayloadBuilder.complete().withTaskId(task.getId()).build());
    }

    public  void queryUser1(String username){
        securityUtil.logInAs(username);
        System.out.print( username+" 拥有任务的数量："+taskService.createTaskQuery().taskOwner(username).count());
        System.out.print(",负责的任务数量："+taskService.createTaskQuery().taskAssignee(username).count());
        System.out.println(",候选的任务数量："+taskService.createTaskQuery().taskCandidateUser(username).count());
    }
    public void queryUser2(String username){
        securityUtil.logInAs(username);
        System.out.print( username+" 拥有任务的数量："+taskService.createTaskQuery().taskOwner(username).count());
        System.out.print(",负责的任务数量："+taskService.createTaskQuery().taskAssignee(username).count());
        System.out.print(",候选的任务数量："+taskService.createTaskQuery().taskCandidateUser(username).count());
        System.out.println(",所有待办任务："+taskService.createTaskQuery().taskCandidateOrAssigned(username).count());
        Page<Task> tasks = taskRuntime.tasks(Pageable.of(0, 100));
        List<Task> list = tasks.getContent();
        for (int i = 0; i < list.size(); i++) {
            Task task = list.get(i);
            if (task.getAssignee() == null) {
                //候选人为当前登录用户，null的时候需要前端拾取
                System.out.println("任务ID："+task.getId()+",任务名称："+task.getName()+",任务负责人：Assignee：待拾取任务");
            } else {
                System.out.println("任务ID："+task.getId()+",任务名称："+task.getName()+",任务负责人："+task.getAssignee());
            }
        }
    }

}
