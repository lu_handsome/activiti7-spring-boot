package com.peng.activiti.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MyDataDao {
    String[][] usersGroupsAndRoles = {
            {"user01", "password", "ROLE_ACTIVITI_USER", "ROLE_COMMON","GROUP_GROUP1"},
            {"user02", "password", "ROLE_ACTIVITI_USER", "ROLE_COMMON","GROUP_GROUP1"},
            {"user03", "password", "ROLE_ACTIVITI_USER", "ROLE_COMMON","GROUP_GROUP2"},
            {"leader01", "password", "ROLE_ACTIVITI_USER","ROLE_LEADER", "GROUP_GROUP3"},
            {"leader02", "password", "ROLE_ACTIVITI_USER", "ROLE_LEADER","GROUP_GROUP3"},
            {"leader03", "password", "ROLE_ACTIVITI_USER", "ROLE_LEADER","GROUP_GROUP4","ADMIN"},
    };
    public UserDetails findUser(String username){
        //模拟从数据中的用户
        List<User> userList = new ArrayList<>();
        List<String> authoritiesStrings1 = Arrays.asList(Arrays.copyOfRange(usersGroupsAndRoles[0], 2, usersGroupsAndRoles[0].length));
        List<String> authoritiesStrings2 = Arrays.asList(Arrays.copyOfRange(usersGroupsAndRoles[1], 2, usersGroupsAndRoles[1].length));
        List<String> authoritiesStrings3 = Arrays.asList(Arrays.copyOfRange(usersGroupsAndRoles[2], 2, usersGroupsAndRoles[2].length));
        List<String> authoritiesStrings4 = Arrays.asList(Arrays.copyOfRange(usersGroupsAndRoles[3], 2, usersGroupsAndRoles[3].length));
        List<String> authoritiesStrings5 = Arrays.asList(Arrays.copyOfRange(usersGroupsAndRoles[4], 2, usersGroupsAndRoles[4].length));
        List<String> authoritiesStrings6 = Arrays.asList(Arrays.copyOfRange(usersGroupsAndRoles[5], 2, usersGroupsAndRoles[5].length));
        userList.add(new User("user01","",
                authoritiesStrings1.stream().map(a -> new SimpleGrantedAuthority(a)).collect(Collectors.toList())));
        userList.add(new User("user02","",
                authoritiesStrings2.stream().map(a -> new SimpleGrantedAuthority(a)).collect(Collectors.toList())));
        userList.add(new User("user03","",
                authoritiesStrings3.stream().map(a -> new SimpleGrantedAuthority(a)).collect(Collectors.toList())));
        userList.add(new User("leader01","",
                authoritiesStrings4.stream().map(a -> new SimpleGrantedAuthority(a)).collect(Collectors.toList())));
        userList.add(new User("leader02","",
                authoritiesStrings5.stream().map(a -> new SimpleGrantedAuthority(a)).collect(Collectors.toList())));
        userList.add(new User("leader03","",
                authoritiesStrings6.stream().map(a -> new SimpleGrantedAuthority(a)).collect(Collectors.toList())));


        Optional<User> user=userList.stream().filter(a -> username.equals(a.getUsername()))
                .findFirst();
//        System.out.println("findUser:"+user);
        return user.orElse(null);
    }

    public List<String[]> list(){
        return Arrays.stream(usersGroupsAndRoles).collect(Collectors.toList());
    }
}
