package com.peng.activiti.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@Primary
public class MyUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private MyDataDao myDataDao;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDetails user = this.myDataDao.findUser(s);
        //返回查找到的用户
        if (user == null){
            throw new UsernameNotFoundException(s);
        }else
        {
            return user;
        }
    }
}
