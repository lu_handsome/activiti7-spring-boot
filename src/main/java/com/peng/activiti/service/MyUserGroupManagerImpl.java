package com.peng.activiti.service;

import org.activiti.api.runtime.shared.identity.UserGroupManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Primary
public class MyUserGroupManagerImpl implements UserGroupManager {
    @Autowired
    private MyDataDao myDataDao;

    @Override
    public List<String> getUserGroups(String s) {
        UserDetails userDetails = this.myDataDao.findUser(s);
//      返回用户GROUPS
        return (List)userDetails.getAuthorities().stream().filter((a) -> {
            return a.getAuthority().startsWith("GROUP_");
        }).map((a) -> {
            return a.getAuthority().substring(6);
        }).collect(Collectors.toList());
    }

    @Override
    public List<String> getUserRoles(String s) {
        UserDetails userDetails = this.myDataDao.findUser(s);
//      返回用户ROLES
        return (List)userDetails.getAuthorities().stream().filter((a) -> {
            return a.getAuthority().startsWith("ROLE_");
        }).map((a) -> {
            return a.getAuthority().substring(6);
        }).collect(Collectors.toList());
    }

    @Override
    public List<String> getGroups() {
        return null;
    }

    @Override
    public List<String> getUsers() {
        return null;
    }
}
